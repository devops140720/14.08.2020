
# prepare the game board list
# loop for game
#       the loop will run until all the board is full,
#       the board may be full only after the x player has played in the 5 round
#       or just prepare a counter and add 1 to it until it is 9
#       or one of the users have won -- check after each gameplay
#   do-while
#  x 0 x
#  0 x 0
#  x 0 x
# ===================================================================

# prepare function that prints the board
def print_board():
    for i in range(3):
        for j in range(3):
            print(board[i][j], end =" ")
        print()

# prepare the board
#board = [ [0,0,0], [0,0,0], [0,0,0]]
board =  [ ['_', '_', '_'] ,
           ['_', '_', '_'],
           ['_', '_', '_'] ]

print_board()
while True:
    row_x = int(input("Player x please enter row [0-2]:"))
    if row_x < 0 or row_x > 2:
        print('must be between 0-2')
    else:
        break

while True:
    col_x = int(input("Player x please enter col [0-2]:"))
    if col_x < 0 or col_x > 2:
        print('must be between 0-2')
    else:
        break
# here we have llegal row and col
# this is a llegal place in the board but it may NOT be empty!
board[row_x][col_x] = 'x'
print_board()
'''

#print_board()

# loop , while the board is not full and no one winns
while True:
    # x player turn
    # present the board
    # loop -- input row and col until row an col are free!!
    #   tell x player to enter a row -- must be a number between 0-2 (loop)
    #   tell x player to enter a column -- must be a number between 0-2 (loop)
    #  after loop ...
    #  we got a free location in the board
    #  put x in the place the player asked
    #  example: row = 0, col = 2
    # board[row][col] = 'x'
    
    # check if x player has won!
    # if x player won ... then print x is the winner + break
    #    if not ... check if board is full -- if so, the game ended in tie -- break
    
    # 0 player turn
    # present the board
    # loop -- input row and col until row an col are free!!
    #   tell 0 player to enter a row -- must be a number between 0-2 (loop)
    #   tell 0 player to enter a column -- must be a number between 0-2 (loop)
    #  after loop ...
    #  we got a free location in the board
    #  put 0 in the place the player asked
    #  example: row = 0, col = 1
    # board[row][col] = '0'
    # check if 0 player has won!
    # if 0 player won ... then print 0 is the winner + break
    break

# if you are here then game is over!
#print('Game Over')
'''